#!/usr/bin/python3

# Programa que pide un número, indica si es
# mayor o menor que 10, y luego escribe desde ese
# número hasta 1, decrementando de uno en uno.

texto = input("Dame un número entero: ")
numero = int(texto)

if numero > 10:
    print("Es mayor que 10")
else:
    print("Es menor o igual a 10")

while numero > 0:
    print(numero)
    numero = numero - 1

print("Fin de el pprograma2")
